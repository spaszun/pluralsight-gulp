var gulp = require('gulp');
var args = require('yargs').argv;
var browserSync = require('browser-sync');
var path = require('path');
var _ = require('lodash');
var config = require('./gulp.config')();
var del = require('del');

//john papa style guide
//https://github.com/johnpapa/angular-styleguide


var $ = require('gulp-load-plugins') ({lazy:true});
//replacing all the plugins with gulp-load-plugin
//you must still install the plugins with
//$npm install --save-dev yargs gulp-load-plugins gulp-if gulp-print gulp-print jshint-stylish gulp-util
//yargs in actualy npm module not gulp plugin

//var jshint = require('gulp-jshint');
//var jscs = require('gulp-jscs');
//var util = require('gulp-util');
//var gulpprint = require('gulp-print');
//var gulpif = require('gulp-if');
//var plumber = require('gulp-plumber'); //Prevent pipe breaking caused by errors from gulp plugins

var port = process.env.PORT || config.defaultPort;

gulp.task('vet', function() {
    log('Analyzing source with jshint and jscs');
    return gulp
        .src(config.alljs) //configuration in external module
        .pipe($.if(args.verbose, $.print())) //prints all the files that is touching based on condition
        .pipe($.jscs())
        .pipe($.jshint())
        .pipe($.jshint.reporter('jshint-stylish', {verbose: true}))
        .pipe($.jshint.reporter('fail')); //to fail is there are some issues with jshint
});

//styles
//compiling css: stylus, less, sass
//css vendor prefixes: postcss/autoprefixer (up to data with caniusesite) -
//prefixes is: when you write transform-origin -> -webkit-transform-origin will be generated
//to add deps: npm install --save-dev gulp-less gulp-autoprefixer
gulp.task('styles', ['clean-styles'], function() {
    log('Comping Less --> CSS');

    return gulp
        .src(config.less)
        .pipe($.plumber()) //will handle errors for all gulp plugins down the pipe
        .pipe($.less())
        //.on('error', errorLogger) //this will log error in case of failure
        .pipe($.autoprefixer({browsers:['last 2 version', '> 5%']}))
        .pipe(gulp.dest(config.temp));
});

//Gulp and html injection - automate adding css and js to html
//wiredep -  inject all bower deps in proper order to HTML (uses <!-- bower:type --><!--endbower-->)
//gulp-inject - injects custom deps into HTML (uses glob pattern for order of deps, uses
// <!-- inject:type --><!-- endinject -->)


gulp.task('clean-styles', function(done) {
    var files = config.temp + '**/*.css';
    clean(files, done);
});

gulp.task('less-watcher', function() {
    gulp.watch([config.less], ['styles']);
});


gulp.task('wiredep', function() {
    log('Wire up the cower css js and our app js into the html');
    var options = config.getWiredepDefaultOptions();
    var wiredep = require('wiredep').stream;

    return gulp
        .src(config.index)//get the index.html
        .pipe(wiredep(options)) //call wiredep to pull bower dependencies
        .pipe($.inject(gulp.src(config.js))) //inject all the custom js files
        .pipe(gulp.dest(config.client));
});
//also connected wiredep to .bowerrc postscript to be invoked while bower instalation:
// bower install --save angular resource

//created new task similar to wiredep to not run styles task every time we do wiredep -
// becouse bower postinstall should be super fast
gulp.task('inject',['wiredep', 'styles', 'templatecache'], function() {
    log('Wire up the app css  into the html, and call wireup');

    return gulp
        .src(config.index)//get the index.html
        .pipe($.inject(gulp.src(config.css))) //inject all the custom css files
        .pipe(gulp.dest(config.client));
});

//serving your dev build - efficiently
//task will prepare code, run the node sever, restart on node changes
//nodemon - node plugin that will restart node server, watch file changes, handle events
// (gulp-nodemon - add support for running a task on fire of the events)
//npm install --save-dev gulp-nodemon
gulp.task('serve-dev',['inject'], function() {
    serve(true, false);
});

//keeping your browser in sync
//browser-sync - injecting file changes (uses socker.io), synchronize actions across browsers
//serve-dev task and startBrowserSync()

//keeping your assets organized
//* task listing and default task - gulp-task-listning
//* coping fonts and images
//  * gulp-imagemin: to minify our images, plugins for additional image processing

gulp.task('help', $.taskListing);
gulp.task('default', ['help']);

gulp.task('fonts', ['clean-fonts'], function() {
    log('Copying the fonts');

    return gulp
        .src(config.fonts)
        .pipe(gulp.dest(config.build + 'fonts'));
});

gulp.task('images', ['clean-images'], function() {
    log('Copying and compressing the images');

    return gulp
        .src(config.images)
        .pipe($.imagemin({optimizationLevel: 4}))
        .pipe(gulp.dest(config.build + 'images'));
});

gulp.task('clean-fonts', function(done) {
    clean(config.build + 'fonts/**/*.*', done);
});

gulp.task('clean-images', function(done) {
    clean(config.build + 'images/**/*.*', done);
});

gulp.task('clean', function(done) {
    var delconfig = [].concat(config.build, config.temp);
    log('Çleaning ' + $.util.colors.blue(delconfig));
    del(delconfig, done);
});

//caching html templates (directive, route, anywhere we get html by url)
//we will warmup the cache using gulp
//$templateCache.put("app/layout/ht-top-nav.html", "<nav></nav>") url/value
//gulp-angular-tempatecache
//(gather all tempates-> minify htnml -> add to tempale cache -> store in android module)

gulp.task('clean-code',  function(done) {
    var files = [].concat(
        config.temp + '**/*.js',
        config.build + '**/*.html',
        config.build + 'js/**/*.js'
    );
    return clean(files, done);
});

gulp.task('templatecache', ['clean-code'], function() {
    log('Creating angular $templateCache');

    return gulp
        .src(config.htmltemplates)
        .pipe($.minifyHtml({empty: true}))
        //gulp-angular-templatecache
        .pipe($.angularTemplatecache(
            config.templateCache.file,
            config.templateCache.options
        ))
        .pipe(gulp.dest(config.temp));
});

//creating a production build pipeline (gulp.userref)
// we gonna:
// * gather the assets in the index html
// * concatenate them into disributable files
// * copy to the build folder
// * update the index.html to point to the new files
//gulp.userref:
// * parses html comments similar to gulp-inject <!-- build:js js/app.js --><!-- endbuild -->
// * contacentes them into one file

//old api v2 now is v3
//.pipe($.useref.assets()) //gathers the assets from html comments
//.pipe($.useref.assets.restore()) //restore the files to the stream - index.html for example
//.pipe($.useref()) //injects contatenated files back to index.html

gulp.task('optimize', ['inject', 'test'], function() {
    log('optimizing the javascript, css, html');

    var assets = $.useref({searchPath: './'});
    var templateCache = config.temp + config.templateCache.file;

    /**
     * to filter in pipes you can use gulp-if  or gulp-filter the choice is yours
    */

    return gulp
        .src(config.index)
        .pipe($.plumber())
        .pipe($.inject(gulp.src(templateCache, {read: false}), { //inject cache
            starttag: '<!-- inject:templates:js -->'
        }))
        //
        .pipe(assets) //gathers the assets from 'build' html comments
        .pipe($.if('**/*.css', $.csso())) //aplying csso
        .pipe($.if(config.optimized.lib, $.uglify()))
        .pipe($.if(config.optimized.app, $.ngAnnotate())) //only for missing explicit angular injections
        .pipe($.if(config.optimized.app, $.uglify()))
        .pipe($.if(['**/*', '!**/index.html'],$.rev()))
        .pipe($.revReplace())
        .pipe(gulp.dest(config.build))
        .pipe($.rev.manifest()) //after everything write manifest
        .pipe(gulp.dest(config.build));

});

gulp.task('serve-build', ['build'], function() {
    serve(false);
});

//minifying and optimyzing
//csso (css optimizer) removes unecessery signs. trnasformations, structural optimalizations (choosing over minify css)
//uglify - for js minifies, removes whitespaces and comments, optionaly mangles js
//add ng-strict-di for checking strict dependencies (always declare what you are injecting)

//angular dependecy injections - uglify can mangle di. solutions:
// *explicit dependency injection using angular
// *using gulp (gulp-ng-annotate) it searching for di ,and add injection code if not found
//     options : add, remove, single_quotes
//     sometimes you must provide hints /*ngInject*/ (see src\client\app\core\config.js)

//handling static assets revision and version bumping (incrementing)
//--Revisions and versions
//* adding file revisions - this is done to update client browser caches when new release comes
//    gulp-rev (rename files by adding content hashes)
//    gulp-rev-replace (rewrite occurrences of filenames that where updated by gulp-rev. for example in html)
//--Bumping versions with the server (gulp-bump plugin)


gulp.task('bump', function() {
    var msg = 'Bumping versios';
    var type = args.type;
    var version = args.version;
    var options = {};
    if (version) {
        options.version = version;
        msg += ' to ' + version;
    } else {
        options.type = type;
        msg += ' to a ' + type;
    }
    log(msg);
    return gulp
        .src(config.packages)
        .pipe($.bump(options))
        .pipe($.print())
        .pipe(gulp.dest(config.root));
});

//testing
// * automate test runner, code coverage, run tests during development, server integration rest, run tests in browsers
// karma - popular test runner
// * helps to hook up to testing frameworks: QUnit, Jasimine, Mocha
// * modes that can be implemented using gulp: single run or  always watching
//npms:
//npm install --save-dev karma karma-chai karma-chai-sinon karma-chrome-launcher karma-coverage
// karma-growl-reporter karma-mocha karma-phantomjs-launcher karma-sinon mocha mocha-clean sinon-chai phantomjs
//sinon - stubing and mocking framework
//mocha - test framework
//chai - assertion lib
//phentomjs - headless browser


gulp.task('test', ['vet', 'templatecache'], function(done) {
    startTests(true /** single run **/, done);
});

gulp.task('autotest', ['vet', 'templatecache'], function(done) {
    startTests(false /** single run **/, done);
});

gulp.task('build', ['optimize', 'fonts', 'images'], function() {
    log('Building everything');

    var msg = {
        title: 'gulp build',
        subtitle: 'Deployed to the build folder',
        message: 'Running gulp serve-build'
    };
    del(config.temp);
    log(msg);
    notify(msg);
});

//integration testing and html test runners
//separate process which will hit the server, we will run the browser not karma
//child_process <- starts process in node

gulp.task('serve-specs', ['build-specs'], function(done) {
   log('server spec runner');
    serve(true /* isDev */, true /* spec runner */);
    done();
});

gulp.task('build-specs', ['templatecache'], function() {
   log('building the spec runner');
    var options = config.getWiredepDefaultOptions();
    var wiredep = require('wiredep').stream;
    var specs = config.specs;

    options.devDependencies = true;

    if (args.startServers) {
        specs = [].concat(specs, config.serverIntegrationSpecs);
    }

    return gulp.src(config.specRunner)
        .pipe(wiredep(options))
        .pipe($.inject(gulp.src(config.testlibraries, {read: false}),
            {name: 'inject:testlibraries'}))
        .pipe($.inject(gulp.src(config.js)))
        .pipe($.inject(gulp.src(config.specHelpers, {read: false}),
            {name: 'inject:spechelpers'}))
        .pipe($.inject(gulp.src(specs, {read: false}),
            {name: 'inject:specs'}))
        .pipe($.inject(gulp.src(config.temp +  config.templateCache.file, {read: false}),
            {name: 'inject:templates'}))
        .pipe(gulp.dest(config.client));
});

//gulp 4
//changes - new task engine:
//  * gulp.series(fn, [fn, ...]) - indetifiesa set of task to run in series (be default gulp runs in parallel):
//      accepts string task name and functions
//      gulp.task('styles', gulp.series('clean-styles', styles)); function styles() {return gulp.src ...}
//  * gulp.parallel(fn, [fn, ...]) - this will run in paralel
//  * mix and match tasks, gulp.series, gulp.parallel: example:
//        gulp.task('build',
//            gulp.series(
//                gulp.parallel('vet', 'test'),
//                gulp.parallel('wiredep', 'styles', 'templatecache'),
//                optimize
//            )
//        );
//installing:
//  * prerelease:
//      npm install --save-dev git://github.com/gulpjs/gulp.git#4.0
//      npm install -g git://github.com/gulpjs/gulp-cli.git#4.0
//migration:
//    task:
//      gulp3 gulp.task(name, [,dep], fn)
//      gulp4 gulp.task(name, fn) - fn is a set of tasks to run in series or
//          parallel or task names (strings and functions)


/////////

function serve(isDev, specRunner) {
    var nodeOptions = {
        script: config.nodeServer,
        delayTime: 1,
        env: {
            'PORT': port,
            'NODE_ENV': isDev ? 'dev' : 'build'
        },
        watch: [config.server]
    };
    return $.nodemon(nodeOptions)
        .on('restart', ['vet'], function(ev) {
            log('*** nodemon restarted ***');
            log('files changed on restart:\n' + ev);
            setTimeout(function() {
                browserSync.notify('restarting now ...');
                browserSync.reload({stream:false});
            }, config.browserReloadDelay); // give server time to reload
        })
        .on('start', function() {
            log('*** nodemon started ***');
            startBrowserSync(isDev, specRunner);
        })
        .on('crash', function() {
            log('*** nodemon crashed: script crashed for some reason');
        })
        .on('exit', function() {
            log('*** nodemon exited cleanly ***');
        });
}

function changeEvent(event) {
    var srcPattern = new RegExp('/.*(?=/' + config.source + ')/');
    log('File ' + event.path.replace(srcPattern, '') + ' ' + event.type);
}

function notify(options) {
    var notifier  = require('node-notifier');
    var notifyOptions = {
        sound: 'Bottle'
    };
    _.assign(notifyOptions, options);
    notifier.notify(notifyOptions);
}

function startBrowserSync(isDev, specRunner) {
    if (args.nosync || browserSync.active) {
        return;
    }
    log('Starting browser-sync on port ' + port);
    if (isDev) {
        gulp.watch([config.less], ['styles'])
            .on('change', function (event) {
                changeEvent(event);
            });
    } else {
        gulp.watch([config.less, config.js, config.html], ['optimize', browserSync.reload])
            .on('change', function (event) {
                changeEvent(event);
            });
    }

    var options = {
        proxy: 'localhost:' + port,
        port: 3000,
        files: isDev ? [ //watched files
            config.client + '**/*.*',
            '!' + config.less,
            config.temp + '**/*.css'
        ] : [],
        ghostMode: { //for sync actions  between many browsers
            clicks: true,
            location: false,
            forms: true,
            scroll: true
        },
        injectChanges: true, //todo this is not working
        logFileChanges: true,
        logLevel: 'debug',
        logPrefix: 'gulp-patterns',
        notify: true, //html pop up inside the browser so you know that is ready
        reloadDelay: 1000
    };

    if (specRunner) {
        options.startPath = config.specRunnerFile;
    }
    browserSync(options);
}

//function errorLogger(error) {
//    log('*** Start of Error ***');
//    log(error);
//    log('*** End of Error ***');
//    this.emit('end');
//}

function startTests(singleRun, done) {
    var child;
    var fork = require('child_process').fork;
    var karma = require('karma').server;
    var serverSpecs = config.serverIntegrationSpecs;
    var excludeFiles = [];
    if (args.startServers) {
        log('Starting server');
        var savedEnv = process.env;
        savedEnv.NODE_ENV = 'dev';
        savedEnv.PORT = 8889;
        child = fork(config.nodeServer);
    } else {
        if (serverSpecs && serverSpecs.length) {
            excludeFiles = serverSpecs;
        }
    }

    karma.start({
        configFile: __dirname + '/karma.conf.js',
        singleRun: !!singleRun, // !! is make sure this is a boolean
        exclude: excludeFiles
    }, karmaCompleted);

    function karmaCompleted(karmaResult) {
        log('Karma completed');
        if (child) {
            log('shutting down the child process');
            child.kill();
        }
        if (karmaResult === 1) {
            done('karma: tests failed with code ' + karmaResult);
        } else {
            done();
        }
    }
}

function clean(path, done) {
    log('Cleaning ' + $.util.colors.blue(path));
    del(path);
    return done();
}

function log(msg) {
    if (typeof(msg) === 'object') {
        for (var item in msg) {
            if (msg.hasOwnProperty(item)) {
                $.util.log($.util.colors.blue(msg[item]));
            }
        }
    } else {
        $.util.log($.util.colors.blue(msg));
    }
}
